document.addEventListener("DOMContentLoaded", (event) => {
    $(document).ready(function() {
        $('#country').on('change', function () {
            $('#loading').removeClass('d-none')
            $.ajax({
                url: 'ajax_oficinas.php',
                method: 'post',
                dataType: 'html',
                data: {
                    city: $(this).val()
                },
                success: (data, status) => {
                    $('#officeCode').html(data);
                },
                complete: () => {
                    setTimeout(
                        () => {
                            $('#loading').addClass('d-none')
                        }, 1000
                    )
                }
            });
        })
    })
})